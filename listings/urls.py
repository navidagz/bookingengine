from django.urls import path, include
from rest_framework import routers

from listings.views import AvailableListingsAPI

ListingRouter = routers.DefaultRouter()
ListingRouter.register("units", AvailableListingsAPI)

urlpatterns = [
    path('', include(ListingRouter.urls))
]
