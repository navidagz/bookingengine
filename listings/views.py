import json

from rest_framework.exceptions import ValidationError
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet

from listings.filtersets import AvailableUnitsFilter
from listings.models import BookingInfo
from listings.pagination import AvailableListingsAPIPagination
from listings.serializers import AvailableListingsAPIISerializer


class AvailableListingsAPI(ListModelMixin, GenericViewSet):
    queryset = BookingInfo.objects.all()
    serializer_class = AvailableListingsAPIISerializer
    pagination_class = AvailableListingsAPIPagination

    def get_queryset(self):
        qs = super(AvailableListingsAPI, self).get_queryset()
        try:
            return AvailableUnitsFilter(**self.request.query_params.dict()).filter_qs(qs)
        except Exception as e:
            raise ValidationError({"detail": json.loads(e.json())})
