from datetime import date
from decimal import Decimal
from typing import Optional

from django.db.models import Count, F, OuterRef, Exists
from pydantic import BaseModel

from listings.models import Reservation, Listing


class AvailableUnitsFilter(BaseModel):
    check_in: date
    check_out: date
    max_price: Optional[Decimal] = None

    def filter_qs(self, queryset):
        queryset = queryset.order_by("price")
        queryset = queryset.select_related("listing", "hotel_room_type", "hotel_room_type__hotel")

        reservations = Reservation.objects.filter(
            check_in__gte=self.check_in,
            check_out__lte=self.check_out
        ).values_list("booking_id", flat=True)

        queryset = queryset.exclude(id__in=reservations)

        if self.max_price:
            queryset = queryset.filter(price__lte=self.max_price)

        hotels = set()
        booking_ids = set()
        for booking in queryset:
            hotel_room_type = booking.hotel_room_type
            if hotel_room_type:
                hotel_id = hotel_room_type.hotel.id
                if hotel_id not in hotels:
                    booking_ids.add(booking.id)
                    hotels.add(hotel_id)
            else:
                booking_ids.add(booking.id)

        return queryset.filter(id__in=booking_ids)
