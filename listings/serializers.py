from rest_framework import serializers


class AvailableListingsAPIISerializer(serializers.Serializer):
    listing_type = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    country = serializers.SerializerMethodField()
    city = serializers.SerializerMethodField()
    price = serializers.DecimalField(max_digits=6, decimal_places=2)

    def get_listing_type(self, obj):
        return obj.get_listing_attr("listing_type").capitalize()

    def get_title(self, obj):
        return obj.get_listing_attr("title")

    def get_country(self, obj):
        return obj.get_listing_attr("country")

    def get_city(self, obj):
        return obj.get_listing_attr("city")

    class Meta:
        ordering = "price"
